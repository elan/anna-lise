#### INSTALL
```bash
git clone https://gitlab.com/litt-arts-num/anna-lise.git
cd anna-lise
cp .env.dist .env
cp db.ini.dist db.ini
docker-compose up -d
docker-compose exec omeka bash
  chmod -R 777 /var/www/html/files /var/www/html/plugins /var/www/html/themes; exit;
#personnaliser le cas échéant config.ini et relancer le docker
docker-compose stop
docker-compose up -d --build
```

#### INSTALL DEV
```bash
git clone https://gitlab.com/litt-arts-num/anna-lise.git
cd anna-lise
cp .env.dist .env
cp db.ini.dist db.ini
docker-compose -f docker-compose-dev.yml up -d
docker-compose -f docker-compose-dev.yml exec omeka bash
  chmod -R 777 /var/www/html/files /var/www/html/plugins /var/www/html/themes; exit;
# personnaliser le cas échéant config.ini et relancer le docker
docker-compose -f docker-compose-dev.yml stop
docker-compose -f docker-compose-dev.yml up -d --build
```

#### USAGE
Go to `URL:8801/install/install.php`

#### SOLR SETUP
In a dev environnement, you can use local solr service (defined in `docker-compose-dev.yml`)
Enable the plugin and use `solr` as Server Host, `8983` as port, and `/solr/omeka-annalise/` as Core URL. Solr Core is mounted as a volume in `docker-files` directory

#### IMPORT PROD. DATA
* sur la prod, dans le dossier anna-lise : `docker-compose exec db /usr/bin/mysqldump -u user --password="password" db > /home/xxx/backup-omeka.sql`
* sur la dev : `scp xxx@ip-server:/home/xxx/backup-omeka.sql .`
* éditer dans le dump la connexion à solr (voir section SOLR SETUP)
*  `cat backup-omeka.sql | docker-compose -f docker-compose-dev.yml exec -T db /usr/bin/mysql -u omeka --password=omeka omeka`

#### NOTES
* config.ini a été configuré pour utiliser le smtp de notre hébergeur
