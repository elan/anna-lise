<?php
$pageTitle = __('Search') . ' ' . __('(%s total)', $total_results);
echo head(array('title' => $pageTitle, 'bodyclass' => 'search'));
$searchRecordTypes = get_search_record_types();
?>
    <div id="left-content">
      <!-- SEARCH CONTAINER -->
      <div id="search-container" role="search">
          <?php if (get_theme_option('use_advanced_search') === null || get_theme_option('use_advanced_search')): ?>
          <?php echo search_form(array('show_advanced' => true)); ?>
          <?php else: ?>
          <?php echo search_form(); ?>
          <?php endif; ?>
      </div>
      <!-- -->
      <div id="left-main-content">
        <?php echo search_filters(); ?>
        <?php if ($total_results): ?>
        <?php echo pagination_links(); ?>
      </div>
    </div>
  
  <!-- -->

  <!-- MAIN CONTENT -->
    <div id="content-margin">
      <div id="content">
<!--      <table id="search-results">-->
          <!--<thead>
              <tr>
                  <th><?php echo __('Record Type');?></th>
                  <th><?php echo __('Title');?></th>
              </tr>
          </thead>
          <tbody>-->
              <?php $filter = new Zend_Filter_Word_CamelCaseToDash; ?>
              <?php foreach (loop('search_texts') as $searchText): ?>
              <?php $record = get_record_by_id($searchText['record_type'], $searchText['record_id']); ?>
              <?php $recordType = $searchText['record_type']; ?>
              <?php set_current_record($recordType, $record); ?>
              <div class="<?php echo strtolower($filter->filter($recordType)); ?>">
                      <h2><a href="<?php echo record_url($record, 'show'); ?>"><?php echo $searchText['title'] ? $searchText['title'] : '[Unknown]'; ?></a></h2>
                  <div class="result-type">
                      <?php echo $searchRecordTypes[$recordType]; ?>
                  </div>
                  <div class="item-meta">
                  <?php if (metadata('item', 'has files')): ?>
                  <div class="item-img">
                      <?php if ($recordImage = record_image($recordType, 'square_thumbnail')): ?>
                          <?php echo link_to($record, 'show', $recordImage, array('class' => 'image')); ?>
                      <?php endif; ?>
                  </div>
                  <?php endif; ?>
              
              
                  </div><!-- end class="item-meta" -->
              </div>



              <?php endforeach; ?>
<!--      </tbody>
      </table>-->
      <?php echo pagination_links(); ?>
      <?php else: ?>
      <div id="no-results">
          <p><?php echo __('Your query returned no results.');?></p>
      </div>
<?php endif; ?>
      </div>
    </div>
<?php echo foot(); ?>
