window.addEventListener('load', (event) => {
  let facettes = document.querySelectorAll('.facette')
  for (var facette of facettes) {
    facetHandler.checkForSingle(facette)
  }

  let filterInputs = document.querySelectorAll('.filter-facette')
  for (var input of filterInputs) {
    input.addEventListener('input', facetHandler.filter);
  }

  let sortBtns = document.querySelectorAll('.btn.sort')
  for (var btn of sortBtns) {
    btn.addEventListener('click', facetHandler.sort);
  }

  // facetHandler.orderByMatch();
});

let facetHandler = {
  orderByMatch() {
    let facettes = document.querySelectorAll('.facette')
    for (var facette of facettes) {
      facette.closest('.accordion-item').dataset.match = facette.querySelectorAll('#solr-facets li.facet-match').length;
    }
    document.querySelector('#panel-type-de-contenu').closest('.accordion-item').dataset.match = 200000

    let ul = document.querySelector("#accordion-facets");
    let li = ul.querySelectorAll(".accordion-item");
    let sorted = Array.from(li).sort((a, b) => {
      return (b.dataset.match) - (a.dataset.match)
    })
    sorted.forEach(e => ul.appendChild(e))
  },
  checkForSingle(facet) {
    let facetName = facet.dataset.facette
    let list = facetHandler.getList(facetName)
    if (list.childElementCount <= 5) {
      document.querySelector('.filters[data-target=' + facetName + ']').remove()
    }
  },

  getList(facet) {
    return document.querySelector('.facette[data-facette=' + facet + ']')
  },

  sort(e) {
    let btn = e.target
    let target = btn.dataset.target
    let criteria = btn.dataset.criteria
    let order = btn.dataset.order
    let list = facetHandler.getList(target)

    Array.from(list.children).sort(function(a, b) {
      let isNumeric = (criteria == 'count')
      if (order == "asc") {
        return a.getAttribute('data-' + criteria).localeCompare(b.getAttribute('data-' + criteria), undefined, {
          numeric: isNumeric
        });
      } else {
        return b.getAttribute('data-' + criteria).localeCompare(a.getAttribute('data-' + criteria), undefined, {
          numeric: isNumeric
        });
      }
    }).forEach(e => list.appendChild(e));

    btn.dataset.order = (order == "asc") ? "desc" : "asc"
  },

  filter(e) {
    let input = e.target
    let value = input.value.toLowerCase()
    let target = input.dataset.target
    let list = facetHandler.getList(target)

    for (var item of list.children) {
      if (item.dataset.value.toLowerCase().includes(value) || value == "") {
        item.classList.remove('d-none')
      } else {
        item.classList.add('d-none')
      }
    }
  }
}
