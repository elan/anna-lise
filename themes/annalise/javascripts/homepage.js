document.addEventListener("DOMContentLoaded", function (event) {
    let itemTypes = document.querySelectorAll('.item-type')
    let options = {
        html: true,
        placement: 'right'
    }

    itemTypes.forEach(itemType => {
        new bootstrap.Tooltip(itemType, options)

        itemType.addEventListener("mouseover", (event) => {
            apply(itemType.dataset.target)
        }, false);

        itemType.addEventListener("mouseout", (event) => {
            reset(itemType.dataset.target)
        }, false);
    })
})

function apply(type) {
    let cartouches = document.querySelectorAll('.cartouche:not(.' + type + ')')
    cartouches.forEach(cartouche => {
        cartouche.classList.add('hideit')
    })

    cartouches = document.querySelectorAll('.cartouche.' + type)
    cartouches.forEach(cartouche => {
        cartouche.classList.remove('hideit')
    })
}

function reset(){
    document.querySelectorAll('.cartouche').forEach(cartouche => {
        cartouche.classList.remove('hideit')
    })
}