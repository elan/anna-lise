Sur cette branche, le thème `/themes/annalise` est modifié :

# Affichage des notices des items

## Template général : `show.php`

* `anna-lise/themes/annalise/items/show.php` appelle les différentes vues pour créer la notice :
  * `<div id="relations">` **TODO** : changer l'intitulé de la relation en fonction du type des items (Films analysés / Analyse de cette Analyse , etc. )
  * `<div id="collection" class="element">` : caché (display:none) dans le CSS `/themes/annalise/css/style.css`
  * `<div id="item-citation" class="element">` : déplacé (dans `show.php`) au bas du panneau latéral de la notice qui contient les métadonnées
  * `<div id="content-full">` **TODO** : ajouter, au dessus de la visionneuse PDF, un bouton *"Voir la mise en page d’origine"*
  * **Et surtout `<?php echo all_element_texts('item'); ?>` : le template par défaut responsable de l'affichage des métadonnées est remplacé par 2 templates, selon les types de contenus :**

## Templates metadata : `common/films-record-metadata.php` / `common/analyses-record-metadata.php`

Le type *Films* appelle `common/films-record-metadata.php`, parce qu'il est vraiment différent (n'a que quelques métadonnées)


Les types *Analyses*, *Analyses d'analyses*, *Théorie de l'analyse* appellent : `common/analyses-record-metadata.php` :
  * Elles ont des propriétés aux noms similaires, et une structuration de notice presque identique
  * Mais en réalité, ces propriétés ne sont pas les mêmes, elles sont rangées dans des Sets d'`Item Type Metadata` distincts.
  * Alors, pour savoir quel Set appeler, il faut d'abord récupérer l'`Item Type` de l'objet dont on affiche la notice, ce que je fais avec :

```php
if (metadata('item', 'item_type_name') == "Analyses")  {
  $targetMetadataSet = "Analyses Item Type Metadata";
  }
if (metadata('item', 'item_type_name') == "Analyses d&#039;analyses")  {
  $targetMetadataSet = "Analyses d'analyses Item Type Metadata";
  }
if (metadata('item', 'item_type_name') == "Théorie de l&#039;analyse")  {
  $targetMetadataSet = "Théorie de l'analyse Item Type Metadata";
  }
```

Une fois qu'on a le set, on s'en sert pour rappeler les propriétés avec

```php
$metadata = "Auteur, autrice du document";  // nom de la propriété
$metadataSet = $targetMetadataSet;          // set auquel elle appartient : les propriétés des différents sets ont heureusement le même nom !
// ...
foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text):
//<div class="element-text">
   echo (__($text));
//</div>
```

Je vous laisse me dire si c'est une bonne idée ou pas de le faire dans le template, s'il vaudrait la peine de l'externaliser :
* soit avec un `include()` comme les propriétés qu'Arnaud a créé pour simplifier les templates
* soit carrément, en récupérant une variable lorsque l'on consulte une première fois l'`Ìtem Type` dans `show.php` (sauf que comme on appelle une fonction de Zend/Omeka qui n'est pas conçue pour, je ne sais pas comment faire.)


* Arnaud m'a créé des fonctions pour simplifier l'affichage des templates, elles sont dans `/themes/annalise/common/display-functions.php`


* **J'ai rendu fixe l'ordre d'affichage des métadonnées dans les notices, à cause des regroupements à faire** : comme il faut écrire dans des <div> (4 ou 5 séparées), il faudrait boucler sur toutes les métadonnées à chaque fois : ça ne me semble pas l'idéal.
  * On ne peut plus changer l'ordre via l'interface d'Omeka.
  * Mais on n'aurait pu changer l'ordre qu'à l'échelle d'une div de 4 ou 5, donc l'intérêt était limité, et les utilisateurs auraient pu courir le risque d'être déconcertés par le système.
* **Je vérifie qu'une métadonnée existe bien (a au moins une valeur) avant de créer sa div**, pour ne pas envoyer en l'air la mise en page avec des <div> vides qui créent des espacements non désirés.
* **Grâce à cela, les petites différences entre les notices ne nécessitent pas plusieurs templates** : les différences de mise en page entre une Analyse demandant d'afficher Revue+ N° ou bien Ouvrage + Auteur de l'ouvrage sont gérées automatiquement puisque, *pour l'instant*, ces propriétés sont mutuellement exclusives. Donc tout va bien.

## Modifs CSS notices `css/style.css`

* J'ai modifié des règles CSS à la fin du fichier, avec des commentaires.
* `display:none` de collection d'un item / de page suivante / page précédente
* Les différents espacements demandés par l'équipe d'Anna-lise entre chaque groupe d'item sont à gérés dans le CSS :
  * pour les grands espacements, avant / après une des 3 divs que j'ai créées
  * pour les plus petits avant / après chaque valeur de MD, en utilisant les ID CSS par défaut (`set`+`nom_de_la_propriété`, ex : `#analyses-item-type-metadata-type-de-texte`)
