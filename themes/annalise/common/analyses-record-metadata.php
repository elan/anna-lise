<?php
// conseil d'Arnaud pour essayer de factoriser un peu.
include("display-functions.php")
?>

<?php
/* Pour commencer : déterminer quel est le l'Item Type de l'Item dont on va afficher les propriétés
 C'est peut-être un peu sale de le mettre ici ? J'aurais bien aimé pouvoir transmettre une var depuis show.php, ou je fais déjà une première vérif. */
if (metadata('item', 'item_type_name') == "Analyses") {
    $targetMetadataSet = "Analyses Item Type Metadata";
}
if (metadata('item', 'item_type_name') == "Analyses d&#039;analyses") {
    $targetMetadataSet = "Analyses d'analyses Item Type Metadata";
}
if (metadata('item', 'item_type_name') == "Théorie de l&#039;analyse") {
    $targetMetadataSet = "Théorie de l'analyse Item Type Metadata";
}
?>

<div id="biblio"> <!-- Div regroupe les informations bibliographiques = 1er bloc de MD demandé -->

  <?php // All basic metadatas are displayed calling the partial template '/common/singlePropertyDisplay.php'
    $metadata = "Auteur, autrice du document";
    $metadataSet = $targetMetadataSet;
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));

    $metadata = "Type de texte";
    $metadataSet = $targetMetadataSet;
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));
  ?>

      <div id="table-biblio">
          <?php // Cas Revue + N°, dans une div sépaciale, pour les afficher sur la même ligne
            $metadata = "Revue";
            $metadataSet =  $targetMetadataSet;
            if (propertyExists($elementsForDisplay, $metadataSet, $metadata) == true):
          ?>
              <div id="<?php labelToId($metadataSet, $metadata); ?>" class="element">
                <h3><?php getLabel($elementsForDisplay, $metadataSet, $metadata) ?></h3>
                <?php foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text): ?>
                  <div class="element-text">
                    <?php echo(__($text)); ?>
                  </div>

                <?php //  Here we store the "Revue Name" to echo a cross search link in the next element div. (Makes no sense to search only the Same "Revue Number", we will echo a link searching Same "Revue Number" AND same "Revue Name").
                $revue = strip_formatting($text, "<em>"); // that's the RAW string value, which was entered with <em></em>, so those tags must be kept (otherwise the search will fail)
                endforeach; ?>
              </div>
          <?php endif; ?>

          <?php
            $metadata = "Numéro";
            $metadataSet =  $targetMetadataSet;
            if (propertyExists($elementsForDisplay, $metadataSet, $metadata) == true):
          ?>
              <div id="<?php labelToId($metadataSet, $metadata); ?>" class="element">
                <h3><?php getLabel($elementsForDisplay, $metadataSet, $metadata) ?></h3>
                <?php foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text): ?>

                  <div class="element-text">
                    <div>
                      <a href="<?php
                       echo '/solr-search?q=' . '&facet=revue_item_type_metadata_s%3A%22' . strip_formatting($revue, "<em>") . '%22+AND+num_ro_item_type_metadata_s%3A%22' .  strip_formatting($text) . '%22'
                     ?>" >
                     <?php echo strip_formatting($text, "<em>"); ?>
                      </a>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
          <?php endif; ?>

      </div> <!-- fin table-biblio pour Revues -->

      <?php //  Les 3 propriétés suivantes n'existent que si l'item n'est pas une revue mais un ouvrage
        $metadata = "Titre de l'ouvrage";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));

        $metadata = "Auteur, autrice de l'ouvrage";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));

        $metadata = "Sous la direction de";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));
      ?>

<!-- Si ouvrage et pas revue, alors ce sont dc:publisher et annalise:collection à réunir sur 1 même ligne -->
      <div id="table-biblio">
        <?php
          $metadata = "Publisher";
          $metadataSet = "Dublin Core";

          echo $this->partial('/common/singlePropertyDisplay.php', array(
                                          'metadata' => $metadata,
                                          'metadataSet' => $metadataSet,
                                          'elementsForDisplay' => $elementsForDisplay,
                                        ));

          $metadata = "Collection";
          $metadataSet =  $targetMetadataSet;
          echo $this->partial('/common/singlePropertyDisplay.php', array(
                                          'metadata' => $metadata,
                                          'metadataSet' => $metadataSet,
                                          'elementsForDisplay' => $elementsForDisplay,
                                        ));
      ?>
      </div> <!-- fin table-biblio pour Ouvrages -->

      <?php
        $metadata = "Année de parution";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));
                                      ?>

</div> <!-- Fin bloc infos biblio -->

<hr/> <!-- Séparation fixe  -->

<?php if (propertyExists($elementsForDisplay, $metadataSet, "Type d'analyse") == true): ?>
<div id="typologie">
  <?php
    $metadata = "Type d'analyse";
    $metadataSet =  $targetMetadataSet;
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));
                                  ?>
</div>
<hr/> <!-- Séparation fixe  -->
<?php endif; ?>

<div id="thematiques"> <!-- Autre grande div : bloc de propriétés décrivant le contenu intellectuel de l'analyse -->

  <?php
    $metadata = "Outil, procédé, paramètre filmique";
    $metadataSet =  $targetMetadataSet;
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));

    $metadata = "Motif, thème";
    $metadataSet =  $targetMetadataSet;
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));

    $metadata = "Concept, invention théorique";
    $metadataSet =  $targetMetadataSet;
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));
                                  ?>
</div><!-- Fin div infos thématiques -->

<?php
$precedingMetadata = array("Outil, procédé, paramètre filmique", "Motif, thème", "Concept, invention théorique");
$metadataSet = $targetMetadataSet;

foreach ($precedingMetadata as $metadata) {
    if (propertyExists($elementsForDisplay, $metadataSet, $metadata)) {
        echo "<hr/>";
        break;
    }
}
  ?>

<div id="contenu-film"> <!-- Nouveau bloc de propriétés, sur la description du contenu du film abordé par l'analyse (se rapporte bien à l'analyse, pas au film)  -->

    <?php
      $metadata = "Personne physique, personne morale";
      $metadataSet =  $targetMetadataSet;
      echo $this->partial('/common/singlePropertyDisplay.php', array(
                                      'metadata' => $metadata,
                                      'metadataSet' => $metadataSet,
                                      'elementsForDisplay' => $elementsForDisplay,
                                    ));

      $metadata = "Personnage";
      $metadataSet =  $targetMetadataSet;
      echo $this->partial('/common/singlePropertyDisplay.php', array(
                                      'metadata' => $metadata,
                                      'metadataSet' => $metadataSet,
                                      'elementsForDisplay' => $elementsForDisplay,
                                    ));

        $metadata = "Genre cinématographique";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));

        $metadata = "Lien avec d'autres arts";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));

        $metadata = "Oeuvre non-cinématographique";
        $metadataSet =  $targetMetadataSet;
        echo $this->partial('/common/singlePropertyDisplay.php', array(
                                        'metadata' => $metadata,
                                        'metadataSet' => $metadataSet,
                                        'elementsForDisplay' => $elementsForDisplay,
                                      ));
    ?>

</div> <!-- Fin contenu intellectuel film -->

<?php
$precedingMetadata = array("Personne physique, personne morale", "Personnage", "Genre cinématographique", "Oeuvre non-cinématographique");
$metadataSet = $targetMetadataSet;
foreach ($precedingMetadata as $metadata) {
    if (propertyExists($elementsForDisplay, $metadataSet, $metadata)) {
        echo "<hr/>";
        break;
    }
}
?>
<div id="notes">

    <?php
      $metadata = "Notes";
      $metadataSet =  $targetMetadataSet;
      if (propertyExists($elementsForDisplay, $metadataSet, $metadata) == true):
    ?>
        <div id="<?php labelToId($metadataSet, $metadata); ?>" class="element">
          <h3><?php getLabel($elementsForDisplay, $metadataSet, $metadata) ?></h3>
          <?php foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text): ?>
          <div class="element-text">
            <?php echo(__($text)); ?>
          </div>
          <?php endforeach; ?>
        </div>

        <hr/>

      <?php endif; ?>
      <?php
        $metadata = "Pour citer";
        $metadataSet =  $targetMetadataSet;
        if (propertyExists($elementsForDisplay, $metadataSet, $metadata) == true):
      ?>
          <div id="<?php labelToId($metadataSet, $metadata); ?>" class="element">
            <h3> Pour citer cette analyse </h3>
            <?php foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text): ?>
            <div class="element-text">
              <?php echo(__($text)); ?>
            </div>
            <?php endforeach; ?>
          </div>

          <hr/>

      <?php endif; ?>

</div>
