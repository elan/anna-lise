<!DOCTYPE html>
<html lang="<?php echo get_html_lang(); ?>">

<head>
    <meta charset="utf-8">
    <link rel="icon" href="/themes/annalise/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if ($description = option('description')): ?>
    <meta name="description" content="<?php echo $description; ?>">
    <?php endif; ?>

    <?php
    if (isset($title)) {
        $titleParts[] = strip_formatting($title);
    }
    $titleParts[] = option('site_title');
    ?>
    <title><?php echo implode(' &middot; ', $titleParts); ?></title>

    <?php echo auto_discovery_link_tags(); ?>

    <!-- Plugin Stuff -->
    <?php fire_plugin_hook('public_head', array('view'=>$this)); ?>


    <!-- Stylesheets -->
    <link href="/themes/annalise/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/themes/annalise/css/style.css" rel="stylesheet" type="text/css">

    <!-- JavaScripts -->
    <?php
    queue_js_file(array(
        'vendor/selectivizr',
        'vendor/jquery-accessibleMegaMenu',
        'vendor/respond',
        'jquery-extra-selectors',
        'seasons',
        'globals',
        'bootstrap.bundle.min'
    ));
    ?>

    <?php echo head_js(); ?>
</head>
<?php echo body_tag(array('id' => @$bodyid, 'class' => @$bodyclass)); ?>
    <div id="wrap">
      <div id="content-header-wrap">
        <header role="banner">
          <div id="title-navigation-search-wrap">
            <div id="title-navigation-wrap">
              <div id="site-title">
                  <?php echo link_to_home_page(theme_logo()); ?>
              </div>


              <nav id="top-nav" class="top" role="navigation">
                  <?php echo public_nav_main(); ?>
              </nav>
            </div>
            <!-- SEARCH CONTAINER - ->
            <div id="search-container" role="search">
                <?php // if (get_theme_option('use_advanced_search') === null || get_theme_option('use_advanced_search')):?>
                <?php // echo search_form(array('show_advanced' => true));?>
                <?php // else:?>
                <?php // echo search_form();?>
                <?php // endif;?>
            </div>
            <!- - -->
          </div>
          <!-- ADMIN BAR -->
          <?php fire_plugin_hook('public_body', array('view'=>$this)); ?>


        </header>
      </div>

        <div id="content_wrapper" role="main" tabindex="-1">
           <!-- background filler div-->
          <div id="background-img"></div>
        
