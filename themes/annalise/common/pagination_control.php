

<?php if ($this->pageCount > 1): ?>


<?php $getParams = $_GET; ?>


<nav aria-label="<?php echo __('Pagination'); ?>">
  <ul class="pagination">
      <?php if (isset($this->previous)): ?>


        <li class="page-item">
          <?php $getParams['page'] = $previous; ?>
          <a class="page-link" href="<?php echo html_escape($this->url(array(), null, $getParams)); ?>">&#60;</a></li>
      <!-- Previous page link -->

      <?php endif; ?>




      <li class="page-input">
      <form action="<?php echo html_escape($this->url()); ?>" method="get" accept-charset="utf-8">
      <?php
      $hiddenParams = array();
      $entries = explode('&', http_build_query($getParams));
      foreach ($entries as $entry) {
          if (!$entry) {
              continue;
          }
          list($key, $value) = explode('=', $entry);
          $hiddenParams[urldecode($key)] = urldecode($value);
      }

      foreach ($hiddenParams as $key => $value) {
          if ($key != 'page') {
              echo $this->formHidden($key, $value);
          }
      }

      // Manually create this input to allow an omitted ID
      $pageInput = '<input type="text"  name="page" title="'
                  . html_escape(__('Current Page'))
                  . '" value="'
                  . html_escape($this->current) . '">';
      echo __('%s / %s', $pageInput, $this->last);
      ?>
      </form>
      </li>



      <?php if (isset($this->next)): ?>
        <?php $getParams['page'] = $next; ?>
        <li class="page-item">
          <a class="page-link" href="<?php echo html_escape($this->url(array(), null, $getParams)); ?>">&#62;</a>
        </li>
      </ul>



      <?php endif; ?>
  </ul>
</nav>

<?php endif; ?>
