<?php
include('display-functions.php');
?>

<div class="block">
    <?php // All basic metadatas are displayed calling the partial template '/common/singlePropertyDisplay.php'
      $metadata = "Titre français";
      $metadataSet = "Films Item Type Metadata";
      echo $this->partial('/common/singlePropertyDisplay.php', array(
                                      'metadata' => $metadata,
                                      'metadataSet' => $metadataSet,
                                      'elementsForDisplay' => $elementsForDisplay,
                                    ));

    $metadata = "Réalisateur, réalisatrice";
    $metadataSet = "Films Item Type Metadata";
    echo $this->partial('/common/singlePropertyDisplay.php', array(
                                    'metadata' => $metadata,
                                    'metadataSet' => $metadataSet,
                                    'elementsForDisplay' => $elementsForDisplay,
                                  ));
    ?>
</div>


<?php
  $metadata = "Année de production";
  $metadataSet = "Films Item Type Metadata";
  echo $this->partial('/common/singlePropertyDisplay.php', array(
                                  'metadata' => $metadata,
                                  'metadataSet' => $metadataSet,
                                  'elementsForDisplay' => $elementsForDisplay,
                                ));

  $metadata = "Pays de production";
  $metadataSet = "Films Item Type Metadata";
  echo $this->partial('/common/singlePropertyDisplay.php', array(
                                  'metadata' => $metadata,
                                  'metadataSet' => $metadataSet,
                                  'elementsForDisplay' => $elementsForDisplay,
                                ));
  ?>

<hr/>
