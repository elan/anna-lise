<?php
function displayPdf($file)
{
    $url = str_replace('http://', 'https://', $file->getWebPath('original'));
    $html = '<object data="'. $url .'#zoom=FitH" type="application/pdf" style="width:100%"></object>';

    return $html;
}

$dcTypeMetadata = ['Dublin Core', 'Type'];
?>

<nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <?php
            $paperPdf = [];
            $couvPdf = [];

            // trick to give a custom label to the tab, depending on the file's type
            $paperPdfLabels["Mise en page Anna-Lise - PDF du traitement de texte"] = "Texte";
            $paperPdfLabels["Mise en page d&#039;origine - PDF du scan OCR"] = "Document Original";

            // if exactly 2 attached files, they must be Papers : we force the array's order
            if (count($item->Files) == 2) {
                foreach ($item->Files as $file) {
                    if (metadata($file, $dcTypeMetadata) == "Mise en page Anna-Lise - PDF du traitement de texte") {
                        $paperPdf[0] = $file;
                    } elseif (metadata($file, $dcTypeMetadata) == "Mise en page d&#039;origine - PDF du scan OCR") {
                        $paperPdf[1] = $file ;
                    }
                }
            } // if exactly 3 attached files, they must be front and backcovers : we force the array's order
              elseif (count($item->Files) == 3) {
                  foreach ($item->Files as $file) {
                      if (metadata($file, $dcTypeMetadata) == "1ère de couverture") {
                          $couvPdf[0] = $file;
                      } elseif (metadata($file, $dcTypeMetadata) == "4ème de couverture") {
                          $couvPdf[1] = $file;
                      } elseif (metadata($file, $dcTypeMetadata) == "Sommaire") {
                          $couvPdf[2] = $file;
                      }
                  }
              } // else ... we just let the array be
              else {
                  foreach ($item->Files as $file) {
                      if (metadata($file, $dcTypeMetadata) == "Mise en page Anna-Lise - PDF du traitement de texte") {
                          array_push($paperPdf, $file);
                      } elseif (metadata($file, $dcTypeMetadata) == "Mise en page d&#039;origine - PDF du scan OCR") {
                          array_push($paperPdf, $file);
                      } elseif (metadata($file, $dcTypeMetadata) == "1ère de couverture") {
                          array_push($couvPdf, $file);
                      } elseif (metadata($file, $dcTypeMetadata) == "4ème de couverture") {
                          array_push($couvPdf, $file);
                      } elseif (metadata($file, $dcTypeMetadata) == "Sommaire") {
                          array_push($couvPdf, $file);
                      }
                  }
              }

        ?>

        <?php if (!empty($couvPdf)): ?>
          <button class="nav-link active" id="<?= 'nav-' . $couvPdf[0]->id . '-tab' ; ?>" data-bs-toggle="tab" data-bs-target="<?= '#nav-' . $couvPdf[0]->id; ?>" type="button" role="tab"
                  aria-controls="<?= $couvPdf[0]->id; ?>" aria-selected="true" title="<?= metadata($couvPdf[0], $dcTypeMetadata); ?>" >
             <?= metadata($couvPdf[0], $dcTypeMetadata); ?>
           </button>
            <?php foreach (array_slice($couvPdf, 1) as $nextPage): // array slice to start from the second file?>
              <button class="nav-link" id="<?= 'nav-' . $nextPage->id . '-tab'; ?>" data-bs-toggle="tab" data-bs-target="<?= '#nav-' . $nextPage->id; ?>" type="button" role="tab" aria-controls="<?= $nextPage->id; ?>" aria-selected="false" title="<?= metadata($nextPage, $dcTypeMetadata); ?>">
                 <?= metadata($nextPage, $dcTypeMetadata); ?>
               </button>
            <?php endforeach; ?>
        <?php elseif (!empty($paperPdf)): ?>
            <button class="nav-link active" id="<?= 'nav-' . $paperPdf[0]->id . '-tab'; ?>" data-bs-toggle="tab" data-bs-target="<?= '#nav-' . $paperPdf[0]->id; ?>" type="button" role="tab" aria-controls="<?= $paperPdf[0]->id; ?>" aria-selected="true" title="<?= $paperPdfLabels[metadata($paperPdf[0], $dcTypeMetadata)]; ?>">
             <?= $paperPdfLabels[metadata($paperPdf[0], $dcTypeMetadata)]; ?>
           </button>
           <?php if (!empty($paperPdf[1])): ?>
             <button class="nav-link" id="<?= 'nav-' . $paperPdf[1]->id . '-tab'; ?>" data-bs-toggle="tab" data-bs-target="<?= '#nav-' . $paperPdf[1]->id; ?>" type="button" role="tab" aria-controls="<?= $paperPdf[1]->id; ?>" aria-selected="false" title="<?= $paperPdfLabels[metadata($paperPdf[1], $dcTypeMetadata)]; ?>">
              <?= $paperPdfLabels[metadata($paperPdf[1], $dcTypeMetadata)]; ?>
            </button>
           <?php endif; ?>
           <div id="lazy-last-filler" style="clear:right;">
           </div>
        <?php endif; ?>
      </div>
</nav>

<div class="tab-content" id="nav-tabContent" style="width:100%; height:100%; /*padding-bottom: 46px !important;*/" >
    <?php if (!empty($couvPdf)): ?>
      <div class="tab-pane fade show active" id="<?= 'nav-' .  $couvPdf[0]->id; ?>" role="tabpanel" aria-labelledby="<?= 'nav-'. $couvPdf[0]->id . '-tab' ; ?>" >
        <?= displayPdf($couvPdf[0]); ?>
      </div>
      <?php foreach (array_slice($couvPdf, 1) as $nextPage): ?>
        <div class="tab-pane fade" id="<?= 'nav-' . $nextPage->id; ?>" role="tabpanel" aria-labelledby="<?= 'nav-'. $nextPage->id . '-tab' ; ?>" >
          <?= displayPdf($nextPage); ?>
        </div>
      <?php endforeach; ?>
   <?php elseif (!empty($paperPdf)): ?>
     <div class="tab-pane fade show active" id="<?= 'nav-' .  $paperPdf[0]->id; ?>" role="tabpanel" aria-labelledby="<?= 'nav-'. $paperPdf[0]->id . '-tab' ; ?>" >
        <?= displayPdf($paperPdf[0]);  ?>
     </div>
        <?php if (!empty($paperPdf[1])): ?>
           <div class="tab-pane fade" id="<?= 'nav-' . $paperPdf[1]->id; ?>" role="tabpanel" aria-labelledby="<?= 'nav-'. $paperPdf[1]->id . '-tab' ; ?>" >
             <?= displayPdf($paperPdf[1]); ?>
           </div>
        <?php endif; ?>
   <?php endif; ?>
  </div>
