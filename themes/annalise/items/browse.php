<?php
$pageTitle = __('Browse Items');
echo head(array('title'=>$pageTitle,'bodyclass' => 'items browse'));
?>



    <div id="left-content">
      <!-- SEARCH CONTAINER -->
      <div id="search-container" role="search">
          <?php if (get_theme_option('use_advanced_search') === null || get_theme_option('use_advanced_search')): ?>
          <?php echo search_form(array('show_advanced' => true)); ?>
          <?php else: ?>
          <?php echo search_form(); ?>
          <?php endif; ?>

      </div>
      <!-- -->
<!--
<h1><?php echo $pageTitle;?> <?php echo __('(%s total)', $total_results); ?></h1>
-->
      <div id="left-main-content">
        <nav class="items-nav navigation secondary-nav" style="margin-left:3%;">
            <?php echo public_nav_items(); ?>
        </nav>

        <?php echo item_search_filters(); ?>
        <?php //echo pagination_links();?>

        <?php if ($total_results > 0): ?>

        <?php
        $sortLinks[__('Title')] = 'Dublin Core,Title';
        $sortLinks[__('Creator')] = 'Dublin Core,Creator';
        $sortLinks[__('Date Added')] = 'added';
        ?>
        <div id="sort-links" style="margin-left:3%;">
            <span class="sort-label"><?php echo __('Sort by: '); ?></span><?php echo browse_sort_links($sortLinks); ?>
        </div>

        <?php endif; ?>
        <?php // echo pagination_links();?>

        <div id="outputs" style="margin-left:3%;">
            <span class="outputs-label"><?php echo __('Output Formats'); ?></span>
            <?php echo output_format_list(false); ?>
        </div>
        <?php fire_plugin_hook('public_items_browse', array('items'=>$items, 'view' => $this)); ?>
      </div>
    </div>
  <!-- MAIN CONTENT -->
    <div id="content-margin">

      <nav id="solr_pagination_top" >
       <h5 id="num-found" >
       <!-- float:left; margin-left: 10%; margin-right:20%; width:30%;  -->
         <?php  echo $results->response->numFound;?> résultats
       </h5>

       <?php echo pagination_links(); ?>

     </nav>

      <!-- <div id="content"> -->
      <div class="card-columns"> <!-- bootstrap cards column  -->




        <?php foreach (loop('items') as $item): ?>
          <?php
          if ($item -> getProperty('item_type_name')) {
              $itemtype = $item ->  getProperty('item_type_name');
          } else {
              $itemtype = "";
          }
          ?>



          <?php if (metadata('item', 'has files')): ?>

            <a href="<?php echo('/items/show/' . $item['id']); ?>"  class="card-link">
                <div class="card <?php echo(text_to_id(str_replace(array("&#039;", "é"), "", $itemtype))); ?>">                   <!-- 1 -->
                    <h5 class="card-title">
                    <?php echo snippet(metadata('item', array('Dublin Core', 'Title')), 0, 75); ?>
                    </h5>
                    <div class="card-body">

                      <!-- <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> -->
                      <?php echo item_image('square_thumbnail', array('class' => "card-img-top"  )); ?>
                    </div>
                </div>
            </a>

          <?php else: ?>

            <a href="<?php echo('/items/show/' . $item['id']); ?>" class="card-link" >
                <div class="card <?php echo(text_to_id(str_replace(array("&#039;", "é"), "", $itemtype))); ?>">
                  <h5 class="card-title">
                    <?php echo snippet(metadata('item', array('Dublin Core', 'Title')), 0, 75); ?>
                  </h5>
                  <div class="card-body">

                    <div class="card-no-img-meta">

                      <?php if ($itemtype == "Films"): ?>
                            <p> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Réalisateur, réalisatrice'))) ;?></p>
                            <p> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Année de production'))) ;?></p>
                            <p> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Pays de production'))) ;?></p>

                      <?php elseif ($itemtype == "Analyses" or $itemtype == "Analyses d&#039;analyses" or $itemtype == "Théorie de l&#039;analyse"): ?>
                          <?php if (metadata('item', array('Item Type Metadata', 'Auteur, autrice du document'))): ?>
                            <p> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Auteur, autrice du document'))) ;?></p>
                          <?php endif; ?>
                          <?php if (metadata('item', array('Item Type Metadata', 'Revue'))): ?>
                            <p> <i><?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Revue'))) . " n° " . strip_formatting(metadata('item', array('Item Type Metadata', 'Numéro')))  ;?></i></p>
                            <p> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Année de parution'))); ?> </p>
                          <?php elseif (metadata('item', array('Item Type Metadata', "Titre de l'ouvrage"))): ?>
                            <p> <i> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', "Titre de l'ouvrage"))); ?> </i></p>
                            <p> <?php echo strip_formatting(metadata('item', array('Item Type Metadata', 'Année de parution'))); ?> </p>
                          <?php endif; ?>
                      <?php endif; ?>

                    </div>
                  </div>
                </div>
              </a>


          <?php endif; ?>

        <?php endforeach; ?>




      </div><!-- end of CARD COLUMNS -->
  </div><!-- end of CONTEN MARGIN ?? WTF ??  -->





<?php echo foot(); ?>
