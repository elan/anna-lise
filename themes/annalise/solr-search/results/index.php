<?php

/**
 * @package     omeka
 * @subpackage  solr-search
 * @copyright   2012 Rector and Board of Visitors, University of Virginia
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 */
 $textQuery = $query["q"]; // text part of the query as entered in the search bar. Facets can be retrieved in $query["facets"]. And sadly that's all.
 ?>
<?php queue_css_file('results'); ?>
<?php queue_js_file('facets'); ?>

<?= head(['title' => __('Solr Search')]); ?>

<div id="left-content">
  <?= $this->partial('/results/facets.php', ['results' => $results, 'textQuery' => $textQuery]) ?>
</div>

<!-- MAIN CONTENT (right main div) -->
<div id="content-margin">
<?= $this->partial('/results/search.php', ['results' => $results, 'query' => $query]) ?>

  <?php
    // Result number
   $numFound = $results->response->numFound;
   // Results display main condition
   if ($numFound > 0):
     echo $this->partial('/results/pagination.php', ['results' => $results, 'query' => $query, 'sortOptions' => $sortOptions]);
    ?>
  <div id="search-content">
    <div class="results-list">
      <?php
      foreach ($results->response->docs as $doc):
        echo $this->partial('/results/item.php', ['doc' => $doc, 'textQuery' => $textQuery, 'results' => $results]);
      endforeach; 
    else: ?>
        <div style="margin:30px; padding:30px 30px 15px 30px; background-color:white;">

        <?php 
        $no_results_text = get_option('solr_search_no_results_text');
        echo (!empty($no_results_text)) ? $no_results_text : __('No results found');
        ?>

      </div>
     <?php endif; ?>
    </div><!-- end of result list -->
  </div> <!--end of "search content" -->
</div> <!-- end of content margin = right main div -->

<?= foot() ?>
