<?php

include "facetLabels.php";

    // Pour que les facettes et la valeur de la recherche plein-texte restent activées d'une requête à l'autre,
    // leurs valeurs sont stockées dans les input du formulaire de recherche :
    $retainedTextQuery = str_replace('"', '', ($_GET['q'])); // en échappant les double quotes qui encadrent la valeur de la recherche plein texte.
    $hiddenInputFacets = htmlentities(stripslashes($query["facet"])); // en échappant les doubles / simple quotes et des caractères spéciaux (: ( ...)) du segment de l'url contenant les facettes

    // $urlEncodedFacet contient les facettes récupérées du segment de l'URL, prêtes à être réinjectées directement dans un nouvel URL conservant les facettes ignorant le segment de la recherche en plein texte
    // (pour annuler la recherche plein texte mais conserver les facettes passées précédement)
    // (variable à part car les échappements nécessaires sont différents de ceux d'un input)
    $urlEncodedFacet = str_replace("'", "%27", $query["facet"]);

?>



<div id="search-container" role="search" style="padding:20px 20px 10px 20px;">
  <form id="solr-search-form" style="padding-top: 0 !important; margin-bottom:30px;">
    <span class="float-wrap">
      <input
          id="solr-search-input"
          type="text"
          placeholder="..."
          title="<?= __('Search keywords') ?>"
          name="q"
          value="<?= array_key_exists('q', $_GET) ? $retainedTextQuery : '' ?>"
      />
      <input type="hidden" name="facet" value="<?= $hiddenInputFacets ?>" />
    </span>
    <input type="submit" value="Recherche" />
  </form>

  <div id="search-applied-facets-wrapper">

    <div id="search-applied-facets-title">
        <?php if (count(SolrSearch_Helpers_Facet::parseFacets()) > 0): ?>
              <h5><b>Filtres appliqués</b></h5>
        <?php endif; ?>
    </div>

  <div id="solr-applied-facets" style="/*width:100%; margin:0 !important;*/"> 
        <ul>
    <?php 

        // AFFICHAGE DES FACETTES ACTIVES
        foreach (SolrSearch_Helpers_Facet::parseFacets() as $facet):

            // special treatment to retrieve all types of analysis together
            // the simplest way to do is so is a negative search excluding "films", and to rewrite the property label
            if ($facet[0] . $facet[1] == "-itemtype" . "Films") {
                $facetlabel = "Type de contenu ";
                $value = "Tous les types d'analyses";
            } else {
                $value = strip_formatting($facet[1]);
                $facetlabel = SolrSearch_Helpers_Facet::keyToLabel($facet[0]);

            }
            $value = str_replace("%27", "'", $value);

        ?>

              <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-start">
                <span class="applied-facet-value"> <?= $facetlabel . ' : ' ?></span>
                <span class="applied-facet-value"> <?= $value ?>            </span>
                <span style="float: right;">
                  <a class="badge bg-dark rounded-pill" href="<?= SolrSearch_Helpers_Facet::removeFacet($facet[0], $facet[1]); ?>">
                      x
                  </a>
                </span>
              </li>
      <?php endforeach;

        // AFFICHAGE DE LA RECHERCHE TEXTUELLE
        if ($query["q"]): ?>
          <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-start" style="display:none !important;">
            <span class="applied-facet-value">
              <?= '"'. str_replace('"', '', $query["q"]) .'"' ?>
            </span>
            <span>
              <a class="badge bg-dark rounded-pill" href='<?= "/solr-search?q=&facet=" . $urlEncodedFacet ?>'>
                  x
              </a>
            </span>
          </li>
        <?php endif;?>

    </ul>
  </div>


  </div>

  

</div>
