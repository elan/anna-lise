<!-- PAGINATION -->
<nav id="solr_pagination_top" >
  <h5 id="num-found" > <?= $results->response->numFound ?> résultats </h5>
  <?= pagination_links() ?>
  <h5 style="margin-top:0; margin-bottom:0;">Trier par</h5>
  <div style="float:right; margin-left:30px; display:inline-block;">
      <?= $this->partial('/results/sort.php', ['query' => $query,'sortOptions' => $sortOptions]) ?>
  </div>
</nav>
