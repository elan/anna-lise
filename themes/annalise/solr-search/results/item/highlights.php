<?php

// Stores facets Labels / facets IDs in an array to call them later, in the desired display order :
$facetTable = [];
$facetTable["itemtype"]= "Type de contenu";
$facetTable["title_dublin_core_t"]= "le titre";
$facetTable["r_alisateur_r_alisatrice_item_type_metadata_t"] ="Réalisateur, réalisatrice";
$facetTable["pays_de_production_item_type_metadata_t"] ="Pays de production";
$facetTable["ann_e_de_production_item_type_metadata_t"] ="Année de production (film)";
$facetTable["auteur_autrice_du_document_item_type_metadata_t"] ="Auteur, autrice du document";
$facetTable["auteur_autrice_de_l_ouvrage_item_type_metadata_t"] ="Auteur, autrice de l'ouvrage";
$facetTable["sous_la_direction_de_item_type_metadata_t"] ="Sous la direction de";
$facetTable["type_de_texte_item_type_metadata_t"] ="Type de texte";
$facetTable["type_d_analyse_item_type_metadata_t"] ="Type d'analyse";
$facetTable["revue_item_type_metadata_t"] ="Revue";
$facetTable["collection_item_type_metadata_t"] ="Collection";
$facetTable["ann_e_de_parution_item_type_metadata_t"] ="Année de parution (analyse)";
$facetTable["titre_de_l_ouvrage_item_type_metadata_t"] ="Titre de l'ouvrage";
$facetTable["outil_proc_d_param_tre_filmique_item_type_metadata_t"] ="Outil, procédé, paramètre filmique";
$facetTable["motif_th_me_item_type_metadata_t"] ="Motif, thème";
$facetTable["concept_invention_th_orique_item_type_metadata_t"] ="Concept, invention théorique";
$facetTable["personne_physique_personne_morale_item_type_metadata_t"] ="Personne physique, personne morale";
$facetTable["personnage_item_type_metadata_t"] ="Personnage";
$facetTable["genre_cin_matographique_item_type_metadata_t"] ="Genre cinématographique";
$facetTable["lien_avec_d_autres_arts_item_type_metadata_t"] ="Lien avec d'autres arts";
$facetTable["oeuvre_non_cin_matographique_item_type_metadata_t"] ="Oeuvre non-cinématographique";
$facetTable["notes_item_type_metadata_t"] ="Notes";
$facetTable["text_pdf_text_t"] = " le texte du document";

// kept but not used
// $facetTable["tag"]= "Tag";
// $facetTable["collection"]= "Collection";
// $facetTable["resulttype"]= "Result Type";
// $facetTable["featured"]= "Mis en avant";
// $facetTable["pour_citer_item_type_metadata_t"] ="Pour citer";
// $facetTable["titre_fran_ais_item_type_metadata_t"] ="Titre français (film)";
// $facetTable["num_ro_item_type_metadata_t"] ="Numéro";

 $matchesCount = count(get_object_vars($results->highlighting->{$doc->id})); // compte les matches des facettes pour le courant dans affichage des résultats

if ($textQuery && $matchesCount >= 1): ?>
  <div>
    <p class="card-text">
      <button class="btn btn-outline-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-matches-<?=$doc->id?>"
       aria-expanded="false" aria-controls="collapse-matches-<?=$doc->id?>">
       <?=$matchesCount?> Occurrence(s) de <em><?=$textQuery?></em>
      </button>

      <div class="collapse" id="collapse-matches-<?=$doc->id?>">
        <?php foreach ($facetTable as $key => $value ) : // boucle sur la liste ordonnée des clef => labels des facettes
                  if ($results->highlighting->{$doc->id}->$key != null):  // si une facette correspond dans les résultats, elle s'affiche (dans le bon ordre)
                          $label = $facetTable[$key];                     // le label
                          $matches = $results->highlighting->{$doc->id}->$key; // la liste des matches dans la liste des résultats.?>

                          <div class="match-facet" >
                              <span>
                              <?= '<b>' . sizeof($matches) . '</b>' . " dans " . $label?>
                              </span>
                              <span>
                                <ul class="list-group list-group-flush">
                                <?php foreach ($matches as $match): // affichage de la liste des matches?>
                                  <li class="list-group-item match">
                                    <?=strip_tags($match, ['<em>', '<mark>'])?>
                                  </li>
                                <?php endforeach; ?>
                                </ul>
                              </span>
                          </div>
          <?php endif;
            endforeach; ?>
      </div>
    </p>
  </div>

<?php endif; // Endif $matchesCount >= 1?>
