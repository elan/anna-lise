<h6 class="card-subtitle mb-2 text-muted">
  <?php
  foreach (metadata($item, ['Item Type Metadata', 'Réalisateur, réalisatrice'], ['all' => true]) as $director) {
      echo strip_formatting($director) . ' ' ;
  }
  ?>
</h6>
<h6 class="card-subtitle mb-2 text-muted">
   <?=strip_formatting(metadata($item, ['Item Type Metadata', 'Année de production']))?>
</h6>
<h6 class="card-subtitle mb-2 text-muted">
  <?php
  foreach (metadata($item, ['Item Type Metadata', 'Pays de production'], ['all' => true]) as $country) {
      echo strip_formatting($country) . ' ' ;
  }
  ?>
</h6>
