<?php if (metadata($item, ['Item Type Metadata', 'Auteur, autrice du document'])): ?>
  <h6 class="card-subtitle mb-2 text-muted">
    <?php
    foreach (metadata($item, ['Item Type Metadata', 'Auteur, autrice du document'], ['all' => true]) as $author) {
        echo strip_formatting($author) . ' ' ;
    } ?>
   </h6>
<?php endif; ?>
<?php if (metadata($item, ['Item Type Metadata', 'Revue'])): ?>
  <h6 class="card-subtitle mb-2 text-muted">
     <i><?=strip_formatting(metadata($item, ['Item Type Metadata', 'Revue'])) . " n° " . strip_formatting(metadata($item, ['Item Type Metadata', 'Numéro']))?></i>
   </h6>
  <h6 class="card-subtitle mb-2 text-muted">
     <?= strip_formatting(metadata($item, ['Item Type Metadata', 'Année de parution']))?>
   </h6>
<?php elseif (metadata($item, ['Item Type Metadata', "Titre de l'ouvrage"])): ?>
  <h6 class="card-subtitle mb-2 text-muted">
     <i> <?= strip_formatting(metadata($item, ['Item Type Metadata', "Titre de l'ouvrage"]))?> </i>
   </h6>
  <h6 class="card-subtitle mb-2 text-muted">
     <?=strip_formatting(metadata($item, ['Item Type Metadata', 'Année de parution']))?>
   </h6>
<?php endif; ?>
